﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; remap caps lock funtionality to scroll lock
SetCapsLockState Off
SL_state:=false
ScrollLock::
	SL_state:= not SL_state
	if (SL_state) {
		SetCapsLockState On
	} else {
		SetCapsLockState Off
	}
	Return

; remap caps lock to behave as ctrl modifier on hold or esc when tapping
*CapsLock::
	Send {LControl down}
	Return
*CapsLock up::
	Send {LControl Up}
	if (A_PriorKey=="CapsLock"){
		if (A_TimeSincePriorHotkey < 1000)
			Suspend On
			Send, {Esc}
			Suspend Off
	}
	Return

; remap holding space behave as shift modifier
*Space::
	SendInput, {Shift Down}
	Return
*Space Up::
	SendInput, {Shift Up}
	if (A_PriorKey=="Space"){
		if (A_TimeSincePriorHotkey < 200) {
			SendInput, %A_Space%
		}
	}
	Return

; this is needed to get the diacritics change bellow to work
*LShift::
	SendInput, {Shift Down}
	Return
*LShift Up::
	SendInput, {Shift Up}
	Return

; make tapping shift behave as BackSpace
RShift:: Send {BackSpace}

#InstallKeybdHook

