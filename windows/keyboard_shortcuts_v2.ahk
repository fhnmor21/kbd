; #Warn  ; Enable warnings to assist with detecting common errors.

SendMode("Input")  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir(A_ScriptDir)  ; Ensures a consistent starting directory.

; remap caps lock funtionality to scroll lock
SetCapsLockState("Off")
SL_state:=false
ScrollLock::
{ ; V1toV2: Added bracket
	SL_state:= not SL_state
	if (SL_state) {
		SetCapsLockState("On")
	} else {
		SetCapsLockState("Off")
	}
	Return

} ; V1toV2: Added Bracket before hotkey or Hotstring

; remap caps lock to behave as ctrl modifier on hold or esc when tapping
*CapsLock::
{ ; V1toV2: Added bracket
	Send("{LControl down}")
	Return
} ; V1toV2: Added Bracket before hotkey or Hotstring

*CapsLock up::
{ ; V1toV2: Added bracket
	Send("{LControl Up}")
	if (A_PriorKey=="CapsLock")
	{
		if (A_TimeSincePriorHotkey < 1000)
			Suspend(true)
			Send("{Esc}")
			Suspend(false)
	}
	Return

} ; V1toV2: Added Bracket before hotkey or Hotstring

; remap holding space behave as shift modifier
*Space::
{ ; V1toV2: Added bracket
	SendInput("{Shift Down}")
	Return
} ; V1toV2: Added Bracket before hotkey or Hotstring

*Space Up::
{ ; V1toV2: Added bracket
	SendInput("{Shift Up}")
	if (A_PriorKey=="Space"){
		if (A_TimeSincePriorHotkey < 200) {
			SendInput(A_Space)
		}
	}
	Return
} ; V1toV2: Added Bracket before hotkey or Hotstring

; make tapping shift behave as BackSpace
;RShift::Send("{BackSpace}")


; open kbd hook
; #InstallKeybdHook

; set up key history length
KeyHistory(100)