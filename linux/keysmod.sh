xmodmap -e 'clear lock'
xmodmap -e 'clear control'
xmodmap -e 'keycode 66 = Control_L'
xmodmap -e 'add control = Control_L Control_R'
xcape -e 'Caps_Lock=Escape'

xmodmap -e 'clear shift'
xmodmap -e "keycode 65 = Shift_R"
xmodmap -e "keycode any = space"
xmodmap -e 'add shift = Shift_L Shift_R'
xcape -e "Shift_R=space"

